package from.santa

import java.net.URL
import java.nio.charset.Charset
import kotlin.math.abs

fun resourceURL(path: String): URL = object {}::class.java.getResource(path)

fun readTextFromResource(path: String, charset: Charset = Charsets.UTF_8): String =
    resourceURL(path).readText(charset)

fun <T> List<T>.withElementAt(element: T, index: Int): List<T> {
    return this.subList(0, index) + listOf(element) + this.subList(index + 1, this.size)
}

class Grid<T>(private val grid: List<List<T>>) {

    data class GridEntry<T>(
        val value: T,
        val rowIndex: Int,
        val columnIndex: Int
    )

    private fun getOrNull(rowIndex: Int, columnIndex: Int): T? {
        return grid.getOrNull(rowIndex)?.getOrNull(columnIndex)
    }

    fun getNeighbors(entry: GridEntry<T>): List<T> {
        return listOfNotNull(
            getOrNull(entry.rowIndex - 1, entry.columnIndex - 1),
            getOrNull(entry.rowIndex - 1, entry.columnIndex),
            getOrNull(entry.rowIndex - 1, entry.columnIndex + 1),
            getOrNull(entry.rowIndex, entry.columnIndex - 1),
            getOrNull(entry.rowIndex, entry.columnIndex + 1),
            getOrNull(entry.rowIndex + 1, entry.columnIndex - 1),
            getOrNull(entry.rowIndex + 1, entry.columnIndex),
            getOrNull(entry.rowIndex + 1, entry.columnIndex + 1),
        )
    }

    fun map(fn: (GridEntry<T>) -> T): Grid<T> {
        return Grid(
            grid.mapIndexed { rowIndex, rows ->
                rows.mapIndexed { columnIndex, value ->
                    fn(GridEntry(value, rowIndex, columnIndex))
                }
            }
        )
    }

    fun count(fn: (T) -> Boolean): Int {
        return grid.sumBy { row ->
            row.count { value ->
                fn(value)
            }
        }
    }

    override fun toString(): String {
        return grid.joinToString("\n") { it.joinToString(" ") }
    }

    override fun equals(other: Any?): Boolean {
        return other is Grid<*> && other.grid == this.grid
    }
}

fun String.toGrid() = Grid(this
    .lines()
    .map {
        it.toList()
    })

fun checkEquals(have: Any?, want: Any?) {
    check(have == want) {
        "Check failed: Wanted $want, but got $have"
    }
}

fun <T> List<T>.indicesOf(item: T): List<Int> {
    val indices = mutableListOf<Int>()
    for (i in this.indices) {
        if (this[i] == item) {
            indices.add(i)
        }
    }
    return indices
}

fun <T> List<List<T>>.transpose(): List<List<T>> {
    val result = mutableListOf<MutableList<T>>()
    for((iIndex, i) in this.withIndex()) {
        for ((aIndex, a) in i.withIndex()) {
            if (aIndex !in result.indices) {
                result.add(mutableListOf())
            }
            result[aIndex].add(this[iIndex][aIndex])
        }
    }
    return result
}

fun Pair<Int, Int>.manhattanDistance() = abs(this.first) + abs(this.second)
fun <T, U, V> Pair<T, U>.mapFirst(fn: (Pair<T, U>) -> V): Pair<V, U> = Pair(fn(this), this.second)
fun <T, U, V> Pair<T, U>.mapSecond(fn: (Pair<T, U>) -> V): Pair<T, V> = Pair(this.first, fn(this))