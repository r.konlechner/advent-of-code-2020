package from.santa

fun main() {
    val input = readTextFromResource("/Day-16.txt").parseTickets()
    val example = """
        class: 1-3 or 5-7
        row: 6-11 or 33-44
        seat: 13-40 or 45-50

        your ticket:
        7,1,14

        nearby tickets:
        7,3,47
        40,4,50
        55,2,20
        38,6,12
    """.trimIndent()
        .parseTickets()

    checkEquals(getOutliers(example), listOf(4, 55, 12))
    println(getOutliers(input).sum())
}

fun getOutliers(tickets: Tickets): List<Int> {
    val allRanges = tickets.rules.values.flatMap { listOf(it.first, it.second) }
    val allValues = tickets.nearby.flatten()

    return allValues.filter { value ->
        allRanges.none { range -> value in range }
    }
}

data class Tickets(
    val rules: Map<String, Pair<IntRange, IntRange>>,
    val mine: List<Int>,
    val nearby: List<List<Int>>
)

fun String.parseTickets(): Tickets {
    val (rules, mine, nearby) = this.split("\n\n")
    val ruleMap = rules.lines().map {
        val (name, ranges) = it.split(": ")
        val (first, second) = ranges.split(" or ")
        fun parseRange(range: String): IntRange {
            val (from, to) = range.split("-")
            return from.toInt()..to.toInt()
        }

        val firstRange = parseRange(first)
        val secondRange = parseRange(second)
        Pair(name, Pair(firstRange, secondRange))
    }.toMap()
    return Tickets(
        ruleMap,
        mine.lines().last().split(",").map { it.toInt() },
        nearby.lines().drop(1).map { it.split(",").map { x -> x.toInt() } })
}