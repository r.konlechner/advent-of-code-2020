package from.santa

fun main() {

    val entries = readTextFromResource("/Day-1.txt")
        .lines()
        .map { it.toInt() }
    println(entries)

    fun <T> List<T>.allPairs() = this
        .flatMapIndexed { iLeft, left ->
            this.mapIndexed { iRight, right ->
                if (iLeft != iRight) {
                    listOf(left, right)
                } else {
                    null
                }
            }.filterNotNull()
        }.distinctBy { it.toSet() }

    val a = "a"
    val b = "b"
    val c = "c"

    println(listOf(a, a, b, c).allPairs())

    val result = entries
        .allPairs()
        .first { (a, b) -> a + b == 2020 }
        .let { (a, b) -> a * b }

    println(result)

    /**
     * Part 2
     */
    fun <T> List<T>.allTriples() = this
        .flatMapIndexed { iLeft, left ->
            this.flatMapIndexed { iMiddle, middle ->
                this.mapIndexed { iRight, right ->
                    if (iLeft != iMiddle || iLeft == iRight || iMiddle == iRight) {
                        listOf(left, middle, right)
                    } else {
                        null
                    }
                }
            }.filterNotNull()
        }

    val resultPart2 = entries
        .allTriples()
        .first { (a, b, c) -> a + b + c == 2020 }
        .let { (a, b, c) -> a * b * c }

    println(resultPart2)
}
