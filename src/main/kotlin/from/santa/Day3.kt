package from.santa

fun main() {

    val map = readTextFromResource("/Day-3.txt")
    println(map)
    fun steps(map: String, rightSteps: Int, downSteps: Int): List<Pair<Int, Int>> {
        map.lines().let { lines ->
            val mapWidth = lines.first().length
            return lines
                .indices
                .mapIndexed { right, down ->
                    Pair(down * downSteps, (right * rightSteps) % mapWidth)
                }
                .takeWhile { (a, _) -> a < lines.size }
        }
    }

    fun countTrees(map: String, rightSteps: Int = 3, downSteps: Int = 1) = steps(map, rightSteps, downSteps)
        .count {
            map.lines()[it.first][it.second] == '#'
        }

    val example = """
..##.........##.........##.........##.........##.........##.......
#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..
.#....#..#..#....#..#..#....#..#..#....#..#..#....#..#..#....#..#.
..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#
.#...##..#..#...##..#..#...##..#..#...##..#..#...##..#..#...##..#.
..#.##.......#.##.......#.##.......#.##.......#.##.......#.##.....
.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#
.#........#.#........#.#........#.#........#.#........#.#........#
#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...
#...##....##...##....##...##....##...##....##...##....##...##....#
.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#
""".trimIndent()

    check(countTrees(example) == 7)
    println(countTrees(map))

    /**
     * Part 2
     */
    val slopes = listOf(Pair(1, 1), Pair(3, 1), Pair(5, 1), Pair(7, 1), Pair(1, 2))

    check(slopes
        .map { (right, down) -> countTrees(example, right, down) }
        .fold(1) { a, b -> a * b } == 336)

    val resultPart2 = slopes
        .map { (right, down) -> countTrees(map, right, down).toLong() }
        .fold(1L) { a, b -> a * b }

    println(resultPart2)
}
