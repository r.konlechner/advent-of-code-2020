package from.santa


fun main() {

    val input = readTextFromResource("/Day-4.txt")
    fun String.parsePassports() = this
        .split("\n\n")
        .map {
            it
                .split("\n")
                .flatMap { i ->
                    i.split(" ")
                }
                .map { pair -> pair.split(":").let { (key, value) -> key to value } }
                .toMap()
        }

    val requiredFields = listOf(
        "byr", // (Birth Year)
        "iyr", // (Issue Year)
        "eyr", // (Expiration Year)
        "hgt", // (Height)
        "hcl", // (Hair Color)
        "ecl", // (Eye Color)
        "pid", // (Passport ID)
    )

    fun Map<String, String>.isValid() = requiredFields.all { required -> this.containsKey(required) }

    val example = """
        ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
        byr:1937 iyr:2017 cid:147 hgt:183cm
        
        iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
        hcl:#cfa07d byr:1929
        
        hcl:#ae17e1 iyr:2013
        eyr:2024
        ecl:brn pid:760753108 byr:1931
        hgt:179cm
        
        hcl:#cfa07d eyr:2025 pid:166559648
        iyr:2011 ecl:brn hgt:59in
        """.drop(1).trimIndent()

    check(example.parsePassports().count { it.isValid() } == 2)
    val passports = input.parsePassports()
    println(passports.count { it.isValid() })

    /**
     * Part 2
     */

    fun isValidHeight(input: String): Boolean {
        val matches = Regex("[0-9]+(in|cm)").matches(input)
        return if (matches) {
            val number = input.dropLast(2).toInt()
            when (input.takeLast(2)) {
                "cm" -> number in 150..193
                "in" -> number in 59..76
                else -> error("Invalid length unit")
            }
        } else {
            false
        }
    }

    val requirements: Map<String, (String) -> Boolean> = mapOf(
        "byr" to { x: String -> x.toIntOrNull() in 1920..2002 },
        "iyr" to { x: String -> x.toIntOrNull() in 2010..2020 },
        "eyr" to { x: String -> x.toIntOrNull() in 2020..2030 },
        "hgt" to { x: String -> isValidHeight(x) },
        "hcl" to { x: String -> Regex("#[0-9a-f]{6}").matches(x) },
        "ecl" to { x: String -> listOf("amb", "blu", "brn", "gry", "grn", "hzl", "oth").contains(x) },
        "pid" to { x: String -> Regex("\\d{9}").matches(x) }
    )

    fun Map<String, String>.isValidPart2(): Boolean {
        val hasFields = requirements.all { required -> this.containsKey(required.key) }
        val adheres = this.all {(key, value) ->
            requirements[key]?.invoke(value) ?: true
        }
        return hasFields && adheres
    }
    println(passports.count { it.isValidPart2() })
}
