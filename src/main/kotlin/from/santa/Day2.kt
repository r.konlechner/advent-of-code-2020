package from.santa

fun main() {

    val entries = readTextFromResource("/Day-2.txt").lines()

    fun String.checkPolicy(): Boolean {
        val min = this
            .takeWhile { it != '-' }
            .toInt()
        val max = this
            .dropWhile { it != '-' }
            .drop(1)
            .takeWhile { it != ' ' }
            .toInt()
        val (char, pw) = this
            .split(' ')
            .let {
                it[1].first() to it[2]
            }
        return pw.count { it == char } in min..max
    }

    println(entries.count { it.checkPolicy() })

    fun String.checkPolicyPart2(): Boolean {
        val pos1 = this
            .takeWhile { it != '-' }
            .toInt()
        val pos2 = this
            .dropWhile { it != '-' }
            .drop(1)
            .takeWhile { it != ' ' }
            .toInt()
        val (char, pw) = this
            .split(' ')
            .let {
                it[1].first() to it[2]
            }
        return (pw[pos1 - 1] == char) xor (pw[pos2 - 1] == char)
    }
    check("1-3 a: abcde".checkPolicyPart2())
    check(!"1-3 b: cdefg".checkPolicyPart2())
    check(!"2-9 c: ccccccccc".checkPolicyPart2())
    println(entries.count { it.checkPolicyPart2() })
}
