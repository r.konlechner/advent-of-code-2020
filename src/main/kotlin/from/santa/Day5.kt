package from.santa

fun main() {

    val seats = readTextFromResource("/Day-5.txt")
        .lines()

    println(seats)

    fun IntRange.half(): Pair<IntRange, IntRange> {
        val avg = (this.last - this.first) / 2
        return Pair(this.first..this.first + avg, this.first + avg + 1..this.last)
    }

    check((0..7).half() == Pair(0..3, 4..7))

    /**
     * Returns (row, column)
     */
    fun decodeSeat(
        seat: String,
        rows: IntRange = 0..127,
        columns: IntRange = 0..7
    ): Pair<Int, Int> {
        return if (seat.isEmpty()) {
            check(rows.count() == 1)
            check(columns.count() == 1)
            Pair(rows.first, columns.first)
        } else {
            when(val c = seat.first()) {
                'F' -> decodeSeat(seat.drop(1), rows.half().first, columns)
                'B' -> decodeSeat(seat.drop(1), rows.half().second, columns)
                'L' -> decodeSeat(seat.drop(1), rows, columns.half().first)
                'R' -> decodeSeat(seat.drop(1), rows, columns.half().second)
                else -> error("Unknown character $c")
            }
        }
    }
    check(decodeSeat("FBFBBFFRLR") == Pair(44, 5))

    fun seatID(row: Int, column: Int): Int = row * 8 + column
    fun seatID(seat: Pair<Int, Int>) = seatID(seat.first, seat.second)

    check(seatID(44, 5) == 357)

    val seatIDs = seats.map { seatID(decodeSeat(it)) }.sorted()
    val maxID = seatIDs.last()
    println(maxID)

    /**
     * Part 2
     */

    println(seatIDs)

    val emptySeat = (seatIDs.first()..seatIDs.last())
        .filter {
            !seatIDs.contains(it)
        }
    println(emptySeat)
}
