package from.santa

fun main() {
    val input = readTextFromResource("/Day-14.txt").lines()

    fun interpret(instructions: List<String>): Map<String, Long> {
        lateinit var mask: String
        val memory = mutableMapOf<String, Long>()
        for (instruction in instructions) {

            when {
                instruction.startsWith("mask") -> {
                    mask = instruction.removePrefix("mask = ")
                    println(mask)
                }
                instruction.startsWith("mem") -> {
                    val (address, value) = instruction.split(" = ")
                    val binaryValue = value.toLong().toString(2).padStart(36, '0')
                    val masked = binaryValue.zip(mask).map {
                        when (it.second) {
                            '0' -> '0'
                            '1' -> '1'
                            'X' -> it.first
                            else -> error("Unknown char '${it.second}'")
                        }
                    }.joinToString("")
                    println("Masked binary: $masked")
                    println("Masked decimal: ${masked.toLong(2)}")
                    memory[address.removePrefix("mem[").removeSuffix("]")] = masked.toLong(2)
                }
                else -> error("Unknown instruction $instruction")
            }
        }
        return memory
    }
    val example = """
        mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
        mem[8] = 11
        mem[7] = 101
        mem[8] = 0
    """.trimIndent()
        .lines()

    checkEquals(interpret(example).values.sum(), 165L)
    println(interpret(input).values.sum())
}
