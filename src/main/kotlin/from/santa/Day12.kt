package from.santa

import kotlin.math.abs

fun main() {

    data class Instruction(
        val direction: Char,
        val units: Int
    )

    fun String.parseInstructions() = this
        .lines()
        .map { Instruction(it.first(), it.drop(1).toInt()) }

    val input = readTextFromResource("/Day-12.txt").parseInstructions()

    fun Char.turnLeft() = when (this) {
        'N' -> 'W'
        'E' -> 'N'
        'S' -> 'E'
        'W' -> 'S'
        else -> error("Unknown direction '$this'")
    }

    fun Char.turnRight() = when (this) {
        'N' -> 'E'
        'E' -> 'S'
        'S' -> 'W'
        'W' -> 'N'
        else -> error("Unknown direction '$this'")
    }

    fun Pair<Int, Int>.manhattanDistance() = abs(this.first) + abs(this.second)

    fun Char.move(units: Int) = when (this) {
        'N' -> Pair(0, units)
        'E' -> Pair(units, 0)
        'S' -> Pair(0, -units)
        'W' -> Pair(-units, 0)
        else -> error("Unknown direction '$this'")
    }

    fun move(
        instructions: List<Instruction>,
        initDirection: Char = 'E',
        initX: Int = 0,
        initY: Int = 0
    ): Pair<Int, Int> {
        var currentDirection = initDirection
        var x = initX
        var y = initY
        for (instruction in instructions) {
            when (instruction.direction) {
                'N' -> y += instruction.units
                'E' -> x += instruction.units
                'S' -> y -= instruction.units
                'W' -> x -= instruction.units
                'F' -> {
                    val (deltaX, deltaY) = currentDirection.move(instruction.units)
                    x += deltaX
                    y += deltaY
                }
                'R' -> {
                    repeat(instruction.units / 90) {
                        currentDirection = currentDirection.turnRight()
                    }
                }
                'L' -> {
                    repeat(instruction.units / 90) {
                        currentDirection = currentDirection.turnLeft()
                    }
                }
                else -> error("Unknown instruction $instruction")
            }
        }
        return Pair(x, y)
    }

    val example = """
        F10
        N3
        F7
        R90
        F11
    """.trimIndent()
        .parseInstructions()

    checkEquals(move(example).manhattanDistance(), 25)
    println(move(input).manhattanDistance())
}
