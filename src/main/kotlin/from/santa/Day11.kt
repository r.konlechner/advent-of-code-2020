package from.santa

fun main() {

    val input = readTextFromResource("/Day-11.txt")
        .toGrid()

    fun Char.isOccuppied() = this == '#'

    fun fillSeats(grid: Grid<Char>): Grid<Char> = grid.map { entry ->
        when (entry.value) {
            '.' -> '.'
            'L' -> if (grid.getNeighbors(entry).count { it.isOccuppied() } == 0) '#' else 'L'
            '#' -> if (grid.getNeighbors(entry).count { it.isOccuppied() } >= 4) 'L' else '#'
            else -> error("Error")
        }
    }

    fun converge(seatLayout: Grid<Char>): Grid<Char> {
        var current = seatLayout
        var next = fillSeats(current)
        while (current != next) {
            current = next
            next = fillSeats(next)
        }
        return next
    }

    val example = """
        L.LL.LL.LL
        LLLLLLL.LL
        L.L.L..L..
        LLLL.LL.LL
        L.LL.LL.LL
        L.LLLLL.LL
        ..L.L.....
        LLLLLLLLLL
        L.LLLLLL.L
        L.LLLLL.LL
    """.trimIndent()
        .toGrid()
    check(converge(example).count { it.isOccuppied() } == 37)
    println(converge(input).count { it.isOccuppied() })
}
