package from.santa

fun main() {

    val groups = readTextFromResource("/Day-6.txt")
        .split("\n\n")

    fun allQuestionsTrueByAnyone(groups: List<String>): Int {
        return groups.sumBy {
            val distinct = it.toList().filter { c -> c != '\n' }.distinct()
            distinct.size
        }
    }

    val exampleGroups = """
        abc

        a
        b
        c

        ab
        ac

        a
        a
        a
        a

        b
    """.trimIndent().split("\n\n")

    check(allQuestionsTrueByAnyone(exampleGroups) == 11)
    println(allQuestionsTrueByAnyone(groups))

    fun allQuestionsTrueByEveryone(groups: List<String>): Int {
        return groups.sumBy {
            val lines = it.lines().map { s -> s.toSet() }
            lines.fold(lines.first(), { a, b -> a.intersect(b) }).size
        }
    }

    check(allQuestionsTrueByEveryone(exampleGroups) == 6)
    println(allQuestionsTrueByEveryone(groups))
}
