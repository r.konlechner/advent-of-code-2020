package from.santa

fun main() {

    val (earliest, arrivals) = readTextFromResource("/Day-13.txt").parse()

    val (exampleEarliest, exampleArrivals) = """
        939
        7,13,x,x,59,x,31,19
    """.trimIndent().parse()


    checkEquals(findEarliestDeparture(exampleEarliest, exampleArrivals), Pair(59L, 5L))
    checkEquals(findEarliestDeparture(exampleEarliest, exampleArrivals).let { (x, y) -> x * y }, 295L)
    val (x, y) = findEarliestDeparture(earliest, arrivals)
    println(x * y)
}

fun String.parse(): Pair<Long, List<Long>> {
    return this.lines().let {
        Pair(
            it.first().toLong(),
            it.last()
                .split(",")
                .mapNotNull { x -> x.toLongOrNull() }
        )
    }
}

fun findEarliestDeparture(earliest: Long, busses: List<Long>): Pair<Long, Long> {
    val bus = busses
        .minByOrNull { bus -> generateSequence(bus) { x -> x + bus }.first { x -> x > earliest } - earliest }!!
    return Pair(bus, generateSequence(bus) { x -> x + bus }.first { x -> x > earliest } - earliest)
}
