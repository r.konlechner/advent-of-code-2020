package from.santa

fun main() {
    val input = readTextFromResource("/Day-15.txt").split(",").map { it.toInt() }
    println(input)
    checkEquals(play(listOf(0, 3, 6)), 436)
    println(play(input))

    /**
     * Part 2
     */
    println(play(input, 30_000_000))
}

fun play(starters: List<Int>, iterations: Int = 2020): Int {
    val spoken = mutableListOf<Int>()
    val spokenIndex = mutableMapOf<Int, MutableList<Int>>()
    repeat(iterations) { turn ->
        val next = if (turn in starters.indices) {
            starters[turn]
        } else {
            val lastSpoken = spoken.last()
            if (spokenIndex[lastSpoken]?.size == 1) {
                0
            } else {
                val (former, latter) = spokenIndex[lastSpoken]!!.takeLast(2)
                latter - former
            }
        }
        spoken.add(next)
        spokenIndex.putIfAbsent(next, mutableListOf())
        spokenIndex[next]!!.add(spoken.size - 1)
        if (spoken.size % 1_000_000 == 0) {
            println("1 Million")
        }
    }
    return spoken.last()
}
