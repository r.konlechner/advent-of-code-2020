package from.santa

fun main() {

    val input = readTextFromResource("/Day-9.txt").lines().map { it.toLong() }

    /**
     * From Day 1
     */
    fun <T> List<T>.allPairs() = this
        .flatMapIndexed { iLeft, left ->
            this.mapIndexed { iRight, right ->
                if (iLeft != iRight) {
                    listOf(left, right)
                } else {
                    null
                }
            }.filterNotNull()
        }.distinctBy { it.toSet() }

    val solution = input.windowed(26).first {
        !it
            .dropLast(1)
            .allPairs()
            .map { (x, y) -> x + y }
            .contains(it.last())
    }.last()

    println(solution)

    /**
     * Part 2
     */
    fun <T> List<T>.allWindows(): List<List<T>> {
        return (2..this.size).flatMap { this.windowed(it) }
    }

    fun List<Long>.findWindow(match: Long): List<Long> {
        return this
            .allWindows()
            .first { it.sum() == match }
    }

    val example = """
        5
        20
        15
        25
        47
        40
        62
        55
        65
        95
        102
        117
        150
        182
        127
        219
        299
        277
        309
        576
    """.trimIndent().lines().map { it.toLong() }

    check(example.findWindow(127L).let { Pair(it.minOrNull()!!, it.maxOrNull()!!) == Pair(15L, 47L) })

    val solutionPart2 = input.findWindow(solution)
    println(solutionPart2.minOrNull()!! + solutionPart2.maxOrNull()!!)
}
