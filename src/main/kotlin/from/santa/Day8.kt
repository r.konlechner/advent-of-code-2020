package from.santa

fun main() {

    val input = readTextFromResource("/Day-8.txt")
    val example = """
        nop +0
        acc +1
        jmp +4
        acc +3
        jmp -3
        acc -99
        acc +1
        jmp -4
        acc +6
    """.trimIndent()

    fun interpret(input: String): Int {
        val instructions = input.lines().filter { it.isNotBlank() }
        var ip = 0
        var acc = 0
        val visited = mutableSetOf<Int>()
        do {
            visited.add(ip)
            val (cmd, arg) = instructions[ip].split(" ")
            when (cmd) {
                "nop" -> {
                    ip++
                }
                "acc" -> {
                    acc += arg.toInt()
                    ip++
                }
                "jmp" -> {
                    ip += arg.toInt()
                }
            }
        } while (!visited.contains(ip) && ip in instructions.indices)
        return acc
    }

    check(interpret(example) == 5)
    println(interpret(input))

    /**
     * Part 2
     */

    fun terminates(instructions: List<String>): Boolean {
        var ip = 0
        var acc = 0
        val visited = mutableSetOf<Int>()
        do {
            visited.add(ip)
            val (cmd, arg) = instructions[ip].split(" ")
            when (cmd) {
                "nop" -> {
                    ip++
                }
                "acc" -> {
                    acc += arg.toInt()
                    ip++
                }
                "jmp" -> {
                    ip += arg.toInt()
                }
            }
        } while (!visited.contains(ip) && ip in instructions.indices)
        return ip !in instructions.indices
    }

    fun findCorrectMutation(program: String): List<String> {
        val instructions = program.lines().filter { it.isNotBlank() }
        return instructions
            .indices
            .asSequence()
            .map {
                val fix = instructions[it].let { instruction ->
                    when {
                        instruction.startsWith("nop") -> instruction.replace("nop", "jmp")
                        instruction.startsWith("jmp") -> instruction.replace("jmp", "nop")
                        else -> instruction
                    }

                }
                instructions.withElementAt(fix, it)
            }
            .filter { it != instructions }
            .first {
                terminates(it)
            }
    }
    check(findCorrectMutation(example)[7].startsWith("nop"))
    println(interpret(findCorrectMutation(input).joinToString("\n")))
}
