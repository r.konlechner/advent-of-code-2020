package from.santa

fun main() {

    val input = readTextFromResource("/Day-10.txt")
        .lines()
        .map { it.toInt() }

    fun jaultDistribution(adapters: List<Int>): Int {
        val chain = (adapters + 0 + (adapters.maxOrNull()!! + 3))
            .sorted()
            .windowed(2)

        return chain.filter {
            it.last() - it.first() == 1
        }.size * chain.filter {
            it.last() - it.first() == 3
        }.size
    }

    val example = """
        28
        33
        18
        42
        31
        14
        46
        20
        48
        47
        24
        23
        49
        45
        19
        38
        39
        11
        1
        32
        25
        35
        8
        17
        7
        9
        4
        2
        34
        10
        3
    """.trimIndent()
        .lines()
        .map { it.toInt() }

    check(jaultDistribution(example) == 220)
    println(jaultDistribution(input))
}
